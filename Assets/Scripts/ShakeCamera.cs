﻿using UnityEngine;
using System.Collections;
public class ShakeCamera : MonoBehaviour {
	

	private Quaternion originRotation;
	
	public float shake_decay;
	
	public float shake_intensity;
	
	private bool shaking;
	
	private Transform _transform;
	

	
	void OnEnable() {
		
		_transform = transform;
		
	}
	
	void Update (){
		Shake ();
		if(!shaking)

			
			return;
		
		if (shake_intensity > 0f){
			

			_transform.localRotation = new Quaternion(
				
				originRotation.x + Random.Range (-shake_intensity,shake_intensity) * .2f,
				
				originRotation.y + Random.Range (-shake_intensity,shake_intensity) * .2f,
				
				originRotation.z + Random.Range (-shake_intensity,shake_intensity) * .2f,
				
				originRotation.w + Random.Range (-shake_intensity,shake_intensity) * .2f);
			
			shake_intensity -= shake_decay;
			
		} else {
			
			Debug.Log("stopped shaking");
			
			shaking = false;
			

			_transform.localRotation = originRotation;
			
		}
		
	}
	
	void Shake(){
		
		if(!shaking) {
			

			originRotation = _transform.localRotation;
			
		}
		
		shaking = true;
		
		shake_intensity = .01f;
		
		shake_decay = 0.002f;
		
	}
	
}