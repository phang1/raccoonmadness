﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class rabbiesMode : MonoBehaviour {

	public GameObject player;
	movement m;
	public Text rtext;
	static public bool rabbies = true;
	static public bool infect = false;
	public float time;
	int rabbiesTimer;
	public GameObject rabbiesButton;
	public GameObject rabbiesTimerButton;
	public Text rabbiesTimerText;
	bool nearEnemy = false;
	
	private float distance;
	private float tempdistance;
	public GameObject[] enemy1s;
	
	private Enemy1AI e1script;
	// Use this for initialization
	void Start () {
		time = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if (rabbies) {

			rabbiesButton.SetActive(true);
			m = gameObject.GetComponent<movement> ();
			GameObject.Find ("Main Camera").GetComponent<ShakeCamera> ().enabled = true;
			m.setSpeed (20);
			rabbiesButton.SetActive (true);
			rtext.text = "You got rabies! While in rabies mode you can run twice as fast, and infect the neighbours by pressing 'E'";

			/*
			enemy1s = GameObject.FindGameObjectsWithTag ("Enemy1");
			
			foreach (GameObject e1 in enemy1s) {
				
				tempdistance = Vector3.Distance(e1.transform.position, player.transform.position);
				//Debug.Log(tempdistance);
				if (tempdistance <= 5)
				{
					//Debug.Log(tempdistance);
					e1script = FindObjectOfType(typeof(Enemy1AI)) as Enemy1AI;
					
					if (rabbies && Input.GetKeyDown (KeyCode.E)) {
						e1script.infect = true;
					}
					
				}
			}
			*/

			rabbiesTimerButton.SetActive (true);
			time = time + Time.deltaTime;
			int timer = (int) time;
			rabbiesTimer = timer;
			rabbiesTimer = 20 - rabbiesTimer;
			rabbiesTimerText.text = " Rabies Timer: " + rabbiesTimer;

		}

		if(time >= 20)
		{
			rabbies = false;
			infect = false;
			GameObject.Find ("Main Camera").GetComponent<ShakeCamera> ().enabled = false;
			m.setSpeed (8);
			rabbiesButton.SetActive(false);
			rabbiesTimerButton.SetActive (false);

		}
	}

	void OnTriggerEnter(Collider other){
		Debug.Log (other.tag);
		if(other.tag == "uninfected"){
			Debug.Log("you're at least getting in here");
			other.gameObject.GetComponent<Enemy1AI>().infect = true;
			other.tag = "infected";

		}
	}
}
