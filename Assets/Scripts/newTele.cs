﻿using UnityEngine;
using System.Collections;

public class newTele : MonoBehaviour {

	public Transform target;
	public Rigidbody player;
	bool inTrigger;
	float time;

	// Use this for initialization
	void Start () {
		inTrigger = false;
	}
	
	// Update is called once per frame
	void Update () {

		if (inTrigger && Input.GetKeyDown (KeyCode.E)) {
			player.position = new Vector3 (target.position.x, player.position.y, target.position.z);
			inTrigger = false;
		}


	
	}


	void OnTriggerEnter(Collider other){
		if (other.gameObject.tag == player.gameObject.tag)
			inTrigger = true;
	}

	void OnTriggerExit(Collider other){
		if (other.gameObject.tag == player.gameObject.tag)
			inTrigger = false;
	}
}
