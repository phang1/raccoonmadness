﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GarbageFoodController : MonoBehaviour {

	int foodAmount;
	bool go = false;
	public Text countText;
	public static int foodCount;
	bool hasRabies;
	public Text canText;
	public GameObject canTextObject;

	// Use this for initialization
	void Start () {
		if (this.tag == "player") {
			DontDestroyOnLoad(this.transform.gameObject);
		}
		if (this.tag == "garbageCan"){
			foodAmount = 20;
		}
		
		if (this.tag == "dumpster"){
			foodAmount = 40;
			
		}

		if (this.tag == "single"){
			foodAmount = 5;
			
		}
		
		if (this.tag == "rabiesCan"){
			foodAmount = 20;
			hasRabies = true;
			
		}

		//foodCount = 0;
		countText.text = " Food Counter: " + foodCount.ToString ();
	}
	

	
	// Update is called once per frame
	void Update () {
		if (go && Input.GetKey (KeyCode.E) && !Input.GetKeyDown (KeyCode.E)) {
			for (float time = Time.deltaTime; time <= Time.deltaTime + foodAmount; time = time + Time.deltaTime) {
				if (this.tag != "single") {
					GameObject.Find ("Player").GetComponentInChildren<SkinnedMeshRenderer> ().enabled = false;
					GameObject.Find ("Player").GetComponent<movement> ().enabled = false;
				}
				foodCount += 1;
				foodAmount -= 1;
				countText.text = " Food Counter: " + foodCount.ToString ();

				if (foodAmount == 0) {
					if (hasRabies) {
						GameObject.Find ("Player").GetComponent<rabbiesMode> ().enabled = true;
					}
					go = false;
					canTextObject.SetActive (false);
					GameObject.Find ("Player").GetComponentInChildren<SkinnedMeshRenderer> ().enabled = true;
					GameObject.Find ("Player").GetComponent<movement> ().enabled = true;
					gameObject.SetActive (false);
					break;
				}
			}
				 
		} else {
			GameObject.Find ("Player").GetComponentInChildren<SkinnedMeshRenderer> ().enabled = true;
			GameObject.Find ("Player").GetComponent<movement> ().enabled = true;
		}

		if (foodCount < 0)
			foodCount = 0;

	}






	void OnTriggerEnter(Collider other){
		if (other.tag == "Player") {
			canTextObject.SetActive(true);
			canText.text = "Hold 'E' to collect food";
			go = true;
		}
	}

	void OnTriggerExit(Collider other){
		if (other.tag == "Player") {
			go = false;
			canTextObject.SetActive(false);
		}
	}




}
