﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;
public class Enemy2AI : MonoBehaviour {


	//public Transform trans;
	public Rigidbody rb;
	public float maxSpeed;
	public float radius;
	public float timeToTarget;

	public Transform target;
	public Transform PatrolPoint1;
	public Transform PatrolPoint2;
	public Transform temptarget;
	public GameObject character;
	public Transform raccoon;
	public bool raccoonVisible;
	public static bool raccoonseen;
	private Vector3 steering;
	private float distance;
	private float tempdistance;
	public bool standby;
	bool moving;
	public bool infect;
	
	public Text countText;
	private Enemy1AI e1script;
	public Animator animator;

	public GameObject[] enemy1s;

	// Use this for initialization
	void Start () {
		moving = false;
		infect = false;
		raccoonVisible = false;
		//character.transform.position = PatrolPoint1.transform.position;

		target = PatrolPoint1;
		
		timeToTarget = 0.25f; 
		distance = 1000f;
		standby = true;

		animator = GetComponentInChildren<Animator> ();

	
	}
	
	// Update is called once per frame
	void Update () {
	
		if (!infect) {

			distance = Vector3.Distance (raccoon.position, character.transform.position);		//Calculate distance between Enemy2 and the raccoon to check if absolutely close
		
			if (distance <= 3f) {
			
				GarbageFoodController.foodCount = GarbageFoodController.foodCount - 1;
				countText.text = " Food Counter: " + GarbageFoodController.foodCount.ToString ();
				//End Condition goes here, i.e. when enemy2 catches raccoon
			
			
			
			}

			/*if (teleport.go == true) {
			
			raccoonVisible = false;
			target = PatrolPoint1;
		}*/
			if ((teleport.onTrigger || teleport.onTrigger2) && Input.GetKeyDown (KeyCode.E)) {
			
				raccoonVisible = false;
				target = temptarget;
			}

			enemy1s = GameObject.FindGameObjectsWithTag ("Enemy1");

			foreach (GameObject e1 in enemy1s) {
		
				tempdistance = Vector3.Distance (e1.transform.position, character.transform.position);
				//Debug.Log(tempdistance);
				if (tempdistance <= 30) {
					//Debug.Log(tempdistance);
					e1script = FindObjectOfType (typeof(Enemy1AI)) as Enemy1AI;

					if (e1script.raccoonVisible == true) {
						target = raccoon;
						raccoonVisible = true;
						raccoonseen = true;
						standby = false;

					}

				}
			}

			if (standby) {
			

				steering = target.position - character.transform.position;
			
				steering.y = 0;	

				if (character.transform.rotation == Quaternion.LookRotation (steering)) {
					if (target == PatrolPoint2)
						target = PatrolPoint1;
					else
						target = PatrolPoint2;
				}
				character.transform.rotation = Quaternion.Lerp (character.transform.rotation, Quaternion.LookRotation (steering), 0.05f);
				animator.SetInteger ("E1Param", 0);

			} else {

				if (character.transform.position == temptarget.position) {
					standby = true;
					target = PatrolPoint1;
				}
				steering = target.position - character.transform.position;
		
				steering.y = 0;										//Direction is only on x-z axis
		
				if (steering.magnitude < radius) {					//Returns none if within radius


					if (raccoonVisible == false) {
						/*if (target == PatrolPoint2)
						target = PatrolPoint1;
					else
						target = PatrolPoint2;*/
					}

					return;
				}


				if (raccoonVisible == true) {
		
				}

				steering /= timeToTarget;
		
				if (steering.magnitude > maxSpeed) {			//If too fast, set to max speed
					steering = steering.normalized;
					steering = steering * maxSpeed;
				}
		
				character.transform.rotation = Quaternion.Lerp (character.transform.rotation, Quaternion.LookRotation (steering), 0.2f);	//Rotates the character to face target, in a smoothed manner
				rb.velocity = steering;		
				animator.SetInteger ("E1Param", 1);

			}
		} else {
			raccoonVisible = false;
			moving = true;
			maxSpeed = 5;
			
			GameObject currentObject = null;
			foreach(GameObject uninfected in GameObject.FindGameObjectsWithTag("uninfected")){
				if(currentObject == null)
					currentObject = uninfected;
				else if(Mathf.Abs(Vector3.Distance(this.transform.position, currentObject.transform.position)) > 
				        Mathf.Abs(Vector3.Distance(this.transform.position, uninfected.transform.position))){
					currentObject = uninfected;
				}
				
				if(Mathf.Abs(Vector3.Distance(this.transform.position, uninfected.transform.position)) < 3){
					if(uninfected.GetComponent<Enemy1AI>().enabled)
						uninfected.GetComponent<Enemy1AI>().infect = true;
					else
						uninfected.GetComponent<Enemy2AI>().infect = true;
					uninfected.tag = "infected";
				}
			}
			
			target  = currentObject.transform;
			
			getSteering();
		}

	}
		
	void getSteering(){
		if (moving) {
			steering = target.position - character.transform.position;
			
			steering.y = 0;										//Direction is only on x-z axis
			
			if (steering.magnitude < radius) {					//Returns none if within radius
				
				
				if (raccoonVisible == false) {
					if (target == PatrolPoint2)
						target = PatrolPoint1;
					else
						target = PatrolPoint2;
				}
				
				return;
			}
			
			steering /= timeToTarget;
			
			if (steering.magnitude > maxSpeed) {			//If too fast, set to max speed
				steering = steering.normalized;
				steering = steering * maxSpeed;
			}
			
			
			
			character.transform.rotation = Quaternion.Lerp (character.transform.rotation, Quaternion.LookRotation (steering), 0.2f);	//Rotates the character to face target, in a smoothed manner
			rb.velocity = steering;	
			
			
			
			
			animator.SetInteger ("E1Param", 1);
		} else {
			rb.velocity = new Vector3 (0, 0, 0);
			animator.SetInteger ("E1Param", 0);
			raccoonVisible = false;
			target = PatrolPoint1;
			moving = true;
		}
	}
	void OnTriggerEnter(Collider player)
	{
		if (player.tag == "Player") {
			target = raccoon;
			raccoonVisible = true;
			raccoonseen = true;
			standby = false;
		}
	}
	void onTriggerExit(Collider player)
	{
		if (player.tag == "Player") {
			raccoonVisible = false;
			standby = false;
			target = PatrolPoint1;
		}

	}

}
