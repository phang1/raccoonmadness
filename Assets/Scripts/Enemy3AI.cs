﻿using UnityEngine;
using System.Collections;

public class Enemy3AI : MonoBehaviour {
	
	//public Transform trans;
	public Rigidbody rb;
	public float maxSpeed;
	public float radius;
	public float timeToTarget;
	
	public Transform target;
	public Transform PatrolPoint1;
	public Transform PatrolPoint2;
	public GameObject character;
	public Transform raccoon;
	public bool raccoonVisible;
	//public static bool raccoonseen;
	private Vector3 steering;
	private float distance;
	public float tempdistance;
	private Enemy2AI e2script;
	
	public bool moving;
	public Animator animator;
	
	public GameObject[] enemy2s;

	// Use this for initialization
	void Start () {
		
		raccoonVisible = false;
		character.transform.position = PatrolPoint1.transform.position;
		
		target = PatrolPoint2;
		
		animator = GetComponentInChildren<Animator> ();
		timeToTarget = 0.25f; 
		
		moving = false;
	}
	
	// Update is called once per frame
	void Update () {
		

		distance = Mathf.Abs(Vector3.Distance (raccoon.position, character.transform.position));		//Calculate distance between Enemy3 and the raccoon to check if absolutely close
		
		if (distance <= 2f) {
			Debug.Log("You should havel ost");
			Application.LoadLevel("GameOver");
			//End Condition goes here, i.e. when enemy3 catches raccoon
			animator.SetInteger ("E1Param", 0);

			
		}

		print ("I am here");
		Debug.Log (Enemy2AI.raccoonseen);
		if(Enemy2AI.raccoonseen == true)
		{
			target = raccoon;
			raccoonVisible = true;
			

			//raccoonseen = true;
		}

		steering = target.position - character.transform.position;
		
		steering.y = 0;										//Direction is only on x-z axis
		
		if (steering.magnitude < radius) {					//Returns none if within radius
			
			
			if(raccoonVisible==false){
				if(target==PatrolPoint2)
					target=PatrolPoint1;
				else
					target=PatrolPoint2;
			}
			
			return;
		}
		//enemy2s = GameObject.FindGameObjectsWithTag ("Enemy2");
		
		//foreach (GameObject e2 in enemy2s) {
			
			//tempdistance = Vector3.Distance(e2.transform.position, character.transform.position);
			//Debug.Log(tempdistance);
			//if (tempdistance <= 25)
			//{
				//Debug.Log(tempdistance);
				//e2script = FindObjectOfType(typeof(Enemy2AI)) as Enemy2AI;

				
			//}
		//}

		steering /= timeToTarget;
		
		if (steering.magnitude > maxSpeed) {			//If too fast, set to max speed
			steering = steering.normalized;
			steering = steering * maxSpeed;
		}
		
		character.transform.rotation = Quaternion.Lerp (character.transform.rotation, Quaternion.LookRotation (steering), 0.2f);	//Rotates the character to face target, in a smoothed manner
		rb.velocity = steering;		
		
		animator.SetInteger ("E1Param", 1);
	}
	
	void OnTriggerEnter(Collider player)
	{
		if (player.tag == "Player") {
			target = raccoon;
			raccoonVisible = true;
			Enemy2AI.raccoonseen = true;
		} else if (player.tag == "infected") {
			Debug.Log("you should be here");
			if(Mathf.Abs(Vector3.Distance(this.transform.position, player.transform.position)) < 5){
				if(player.GetComponent<Enemy1AI>().enabled)
					player.GetComponent<Enemy1AI>().infect = false;
				else
					player.GetComponent<Enemy2AI>().infect = false;
				player.tag = "uninfected";
			}

		}
		/*
		if (player.tag == "People") {
			pscript = FindObjectOfType(typeof(PeopleAI)) as PeopleAI;
			if(pscript.isRabies==true){
				target = people;

			}
		
		}*/
	}
	void onTriggerExit(Collider player)
	{rb.velocity = new Vector3 (0, 0, 0);
		animator.SetInteger ("E1Param", 0);
		if (player.tag == "Player") {
			raccoonVisible = false;
			target = PatrolPoint1;
		}
		
	}
	
}
