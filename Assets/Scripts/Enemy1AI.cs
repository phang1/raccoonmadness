﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;
public class Enemy1AI : MonoBehaviour {


	//public Transform trans;
	public Rigidbody rb;
	public float maxSpeed;
	public float radius;
	public float timeToTarget;

	public Transform target;
	public Transform PatrolPoint1;
	public Transform PatrolPoint2;
	public GameObject character;
	public Transform raccoon;
	public bool raccoonVisible;
	private Vector3 steering;
	//public static bool raccoonseen;
	public float distance;
	public bool infect;
	public bool moving;
	public Animator animator;
	GameObject[] uninfected;

	public Text countText;
	private float tempdistance;  

	// Use this for initialization
	void Start () {
	
		maxSpeed = 7;
		raccoonVisible = false;
		character.transform.position = PatrolPoint1.transform.position;

		target = PatrolPoint2;
		
		timeToTarget = 0.25f; 
		distance = 1000f;
		
		animator = GetComponentInChildren<Animator> ();
		infect = false;
		moving = true;
	}
	
	// Update is called once per frame
	void Update () {
	
		if (infect == false) {
			distance = Vector3.Distance (raccoon.position, character.transform.position);		//Calculate distance between Enemy1 and the raccoon to check if absolutely close

			if (distance <= 1f) {
		
				//End Condition goes here, i.e. when enemy1 catches raccoon
				GarbageFoodController.foodCount = GarbageFoodController.foodCount - 1;
				countText.text = " Food Counter: " + GarbageFoodController.foodCount.ToString ();
				moving = false;
			}

			if ((teleport.onTrigger || teleport.onTrigger2) && Input.GetKeyDown (KeyCode.E)) {
		
				raccoonVisible = false;
				target = PatrolPoint1;
			}

			getSteering();

		} else {
		
//			rb.velocity = new Vector3 (0, 0, 0);
//			animator.SetInteger ("E1Param", 0);
			raccoonVisible = false;
			moving = true;
			maxSpeed = 6;


			GameObject currentObject = null;
			foreach(GameObject uninfected in GameObject.FindGameObjectsWithTag("uninfected")){
				if(currentObject == null){
					if(uninfected == null){
						break;
					}
					currentObject = uninfected;

				}
				else if(Mathf.Abs(Vector3.Distance(this.transform.position, currentObject.transform.position)) > 
				        Mathf.Abs(Vector3.Distance(this.transform.position, uninfected.transform.position))){
					currentObject = uninfected;
				}

				if(Mathf.Abs(Vector3.Distance(this.transform.position, uninfected.transform.position)) < 3){
					if(uninfected.GetComponent<Enemy1AI>().enabled)
						uninfected.GetComponent<Enemy1AI>().infect = true;
					else
						uninfected.GetComponent<Enemy2AI>().infect = true;
					uninfected.tag = "infected";
				}
			}

			Debug.Log(currentObject.name);
			target  = currentObject.transform;

			getSteering();
		}


	}

	void getSteering(){
		if (moving) {
			steering = target.position - character.transform.position;
			
			steering.y = 0;										//Direction is only on x-z axis
			
			if (steering.magnitude < radius) {					//Returns none if within radius
				
				
				if (raccoonVisible == false) {
					if (target == PatrolPoint2)
						target = PatrolPoint1;
					else
						target = PatrolPoint2;
				}
				
				return;
			}
			
			steering /= timeToTarget;
			
			if (steering.magnitude > maxSpeed) {			//If too fast, set to max speed
				steering = steering.normalized;
				steering = steering * maxSpeed;
			}
			
			
			
			character.transform.rotation = Quaternion.Lerp (character.transform.rotation, Quaternion.LookRotation (steering), 0.2f);	//Rotates the character to face target, in a smoothed manner
			rb.velocity = steering;	
			
			
			
			
			animator.SetInteger ("E1Param", 1);
		} else {
			rb.velocity = new Vector3 (0, 0, 0);
			animator.SetInteger ("E1Param", 0);
			raccoonVisible = false;
			target = PatrolPoint1;
			moving = true;
		}
	}
		
	void OnTriggerEnter(Collider player)			//Checks if raccoon entered enemy1s trigger area
	{
		if (player.tag == "Player") {
			target = raccoon;
			raccoonVisible = true;
			Debug.Log(raccoonVisible);
			//raccoonseen = true;
		}
	}
	void onTriggerExit(Collider player)				//Checks if raccoon exited enemy1s trigger area
	{
		if (player.tag == "Player") {
			raccoonVisible = false;
			Debug.Log(raccoonVisible);
			//raccoonseen = false;
			target = PatrolPoint1;
		}

	}
}
