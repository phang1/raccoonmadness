﻿using UnityEngine;
using System.Collections;

public class RaccoonControl : MonoBehaviour {


	public float speed;
	private Vector3 moveDirection = Vector3.zero;


	public Animator animator;
	

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {

		CharacterController controller = GetComponent<CharacterController> ();

		moveDirection = new Vector3 (Input.GetAxis ("Horizontal"), 0, Input.GetAxis ("Vertical"));

		Vector3 toTarget = moveDirection - transform.position;
		//toTarget = new Vector3 (0, toTarget.y, 0);
		//transform.rotation = Quaternion.Lerp (transform.rotation, Quaternion.LookRotation (toTarget), 0.2f);

		moveDirection = transform.TransformDirection (moveDirection);
		moveDirection *= speed;
		
		controller.Move (moveDirection * Time.deltaTime);
		
		if (Input.GetKey("up") || Input.GetKey("down") || Input.GetKey("left") || Input.GetKey("right"))
		{

		animator.SetInteger("AnimParameter",1);}
		else	{
			animator.SetInteger("AnimParameter",0);
		}

	
	}
}
