﻿using UnityEngine;
using System.Collections;

public class rotate : MonoBehaviour {
	//raccoon is the super object
	public Rigidbody raccoon;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {



		float speed = new Vector3(raccoon.velocity.x, 0, raccoon.velocity.z).magnitude;
		//anim.SetFloat("Speed", speed);
		//if the speed is greater than 2, than face the direction which you move
		if (raccoon.velocity.magnitude > 1) {

			Rotating(raccoon.velocity.x, raccoon.velocity.z);
		}
	}

	//takes in the two parameters along the plane to create the direction of which to rotate
	void Rotating(float horizontal, float vertical){
		Vector3 targetDirection = new Vector3 (horizontal, 0f, vertical);
		
		Quaternion targetRotation = Quaternion.LookRotation (targetDirection);
		
		Quaternion newRotation = Quaternion.Lerp (this.transform.rotation, targetRotation, 15*Time.deltaTime);
		
		this.transform.rotation = newRotation;
		
	}
}
