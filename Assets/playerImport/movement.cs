﻿using UnityEngine;
using System.Collections;

public class movement : MonoBehaviour {

	//needs to be the super object
	public Rigidbody raccoon;
	//needs to be the main camera
	//camera needs to be a child of the super object
	public Camera myCamera;

	public int speed;
	float moveH, moveV;
	Vector3 forwardDirection, sideDirection, direction;
	float h, v;

	
	public Animator animator;
	// Use this for initialization
	void Start () {
		animator = GetComponentInChildren<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {

		direction = Vector3.zero;

		h = Input.GetAxis("Horizontal");
		v = Input.GetAxis("Vertical");

		Vector3 newForward = myCamera.transform.forward;
		newForward.y = 0;

		Vector3 movement = Vector3.Normalize (v * newForward + h * myCamera.transform.right) * speed;

		if (Input.GetKey("w") || Input.GetKey("a") || Input.GetKey("s") || Input.GetKey("d"))
		{
			
			animator.SetInteger("AnimParameter",1);}
		else	{
			animator.SetInteger("AnimParameter",0);
		}

		//following two lines rotates the camera
		if (Input.GetKey(KeyCode.RightArrow)) {
			myCamera.transform.RotateAround(raccoon.transform.position, Vector3.up, 50*Time.deltaTime);
		}

		if (Input.GetKey (KeyCode.LeftArrow)) {
			myCamera.transform.RotateAround(raccoon.transform.position, Vector3.up, -50*Time.deltaTime);
		}

		//following two if statements control movement relative to the camera's forward
//		if (Input.GetButton ("w")) {
//			forwardDirection = myCamera.transform.forward;
//		} else if (Input.GetButton ("s")) {
//			forwardDirection = -myCamera.transform.forward;
//		} else
//			forwardDirection = Vector3.zero;
//			
//
//		if (Input.GetButton ("a")) {
//			sideDirection = -myCamera.transform.right;
//		} else if (Input.GetButton ("d")) {
//			sideDirection = myCamera.transform.right;
//		} else
//			sideDirection = Vector3.zero;
//	
//		//set y movement to 0
//		direction = forwardDirection + sideDirection;
//		Debug.Log ("forward: " + forwardDirection + " side: " + sideDirection + " direction: " + direction);
//		direction.y = 0f;
//
//		//assign the velocity if it exists
//		if (direction != Vector3.zero) {
//			raccoon.velocity = direction * speed;
//		} 
			
		raccoon.velocity = movement;

	}

	public void setSpeed(int sp)
	{
		speed = sp;
	}

}
